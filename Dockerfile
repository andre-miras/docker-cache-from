FROM ubuntu:18.04

# ENV HOME_DIR="/home/user"

RUN echo this gets cached
RUN echo also that is > /tmp/cached.txt
RUN ls -la /tmp \
    && sha1sum /tmp/*
# but COPY busts the cache
COPY Makefile /tmp/Makefile
RUN ls -la /tmp \
    && sha1sum /tmp/*
# WORKDIR ${HOME_DIR}
# COPY . ${HOME_DIR}
