REGISTRY=andremiras
IMAGE=docker-cache-from
TAG=latest
IMAGE_TAG=$(IMAGE):$(TAG)
REGISTRY_IMAGE_TAG=$(REGISTRY)/$(IMAGE_TAG)

docker/pull:
	docker pull $(REGISTRY_IMAGE_TAG)

docker/build:
	docker build --cache-from=$(REGISTRY_IMAGE_TAG) --tag=$(IMAGE_TAG) .

docker/run:
	docker run -it --rm $(IMAGE_TAG)
