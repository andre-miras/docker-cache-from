# README

[![Build Status](https://secure.travis-ci.org/AndreMiras/docker-cache-from.png?branch=master)](http://travis-ci.org/AndreMiras/docker-cache-from)
[![Docker Build](https://img.shields.io/docker/cloud/build/andremiras/etherollapp-linux)](https://hub.docker.com/r/andremiras/docker-cache-from)

Demonstrates docker `--cache-from` issue, where the cache seems to get busted at `COPY` time.

## Usage
Pull the image:
```sh
make docker/pull
```
Then try to build it locally using `--cache-from` on the freshly pulled image.
```sh
make docker/build
```
See that the cache is not being used for the `COPY` command.

## TODO
- setup Travis showing it doesn't pick up the cache either
- show the docker history/inspect
